<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class GameRoom extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'game_room_id',
    ];

//    public function playerOne()
//    {
//        return $this->hasOne('App\Player', 'player_one_id');
//    }
//    public function playerTwo()
//    {
//        return $this->hasOne('App\Player', 'player_two_id');
//    }


    public function users() {
        return $this->belongsToMany(User::class)->withTimestamps();
    }
    public function messages() {
        return $this->hasMany(Message::class);
    }
}
