<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
//    use HasFactory;
    protected $fillable = [
        'message',
        'game_room_id'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function game_room() {
        return $this->belongsTo(GameRoom::class, "game_room_id");
    }
}
