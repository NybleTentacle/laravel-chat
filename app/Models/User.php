<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use \App\Models\GameRoom;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function game_rooms()
    {
        return $this->belongsToMany(GameRoom::class)->withTimestamps();
    }

    public function messages() {
        return $this->hasMany(Message::class);
    }

    public function gamesWherePlayerOne() {
        return $this->belongsToMany(GameRoom::class, "player_one_id");
    }
    public function gamesWherePlayerTwo() {
        return $this->belongsToMany(GameRoom::class, "player_two_id");
    }

    // Scope
    public function scopeGames($query, $id)
    {
        return $query->where('player_one_id', '=', $id)->orWhere('player_two_id', '=', $id);
    }
}
