<?php

namespace App\Http\Controllers;

use App\Events\MessageSentEvent;
use App\Models\Message;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $game_room_id = $request->query('roomId');
        return Message::with('user')->where('game_room_id', $game_room_id)->get();
    }

    public function store(Request $request)
    {
        $user = Auth::user();
        $game_room_id = $request->input('roomId');
        $userRoom = $user->game_rooms->find($game_room_id);
        if(!$userRoom) {
            return ['error' => 'You are not a member of this room'];
        }
        $message = $user->messages()->create([
            'message' => $request->input('message'),
            'game_room_id' => $game_room_id
        ]);

        // send event to listeners
        broadcast(new MessageSentEvent($message, $user))->toOthers();

        return [
            'message' => $message,
            'user' => $user,
        ];
    }
}
