<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\GameRoom;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Builder;
class GameRoomController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return GameRoom::all()->where("active", true);
    }

    public function update(Request $request, $game_room_id)
    {
        $user = Auth::user();
        $game_room = GameRoom::findOrFail($game_room_id);

        $game_room->users()->syncWithoutDetaching([$user->id]);
        return $game_room;
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        $room = new GameRoom();
        $room->name = 'ChatRoom-' .$room->id;
        $room->active = true;
        $room->max_players = 2;
        $room->max_playesr = 2;
        $room->save();
        $room->users()->sync([$user->id]);
        return $room;
    }

    public function destroy(Request $request, $id)
    {
        $gameRoom = GameRoom::findOrFail($id);
        $gameRoom->active = false;
        $gameRoom->save();
        return $gameRoom;
    }
}
