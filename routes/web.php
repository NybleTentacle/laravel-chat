<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\GameRoomController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//
//Route::view('/', 'chat')->middleware('auth');
//Route::resource('messages', 'MessageController')->only([
//    'index',
//    'store'
//]);


//Route::get('/home', 'HomeController@index')->name('home');
Route::view('/', 'chat')->middleware('auth');
Route::resource('messages', MessageController::class)->only([
    'index',
    'store'
]);

Route::resource('gamerooms', GameRoomController::class)->only([
    'index',
    'store',
    'destroy',
    'update'
]);

//Router::delete
