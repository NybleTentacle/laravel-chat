<?php

use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.Models.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('chat{id}', function ($user, $id) {
//    $game_room = \App\Models\GameRoom::findOrNew($user->game_room_id);
//    if(!\Illuminate\Support\Facades\Auth::check()) {
//        return false;
//    }
    return \Illuminate\Support\Facades\Auth::check();
//    return $user->id === $game_room->player_one_id ||  $user->id === $game_room->player_two_id;
});
