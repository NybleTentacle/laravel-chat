/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// console.log(files.keys().map(key => key.split('/').pop().split('.')[0]))
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
import GameRoomListCompoment from "./components/Game/GameRoomListCompoment";
import GameChatComponent from "./components/GameChatComponent";
import GameComponent from "./components/Game/GameComponent";

Vue.component('game-component', GameComponent);
Vue.component('game-chat-component', GameChatComponent)
Vue.component('game-room-list-component', GameRoomListCompoment);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// const app = new Vue({
//     el: '#app',
// });

const app = new Vue({
    el: '#app',
    data: {
        joinedGame: true
    },
    mounted: function() {
        console.log("APP LOADED")
    }
});
