<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGameRoomToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_room_user', function (Blueprint $table) {
            //
            $table->primary(['user_id', 'game_room_id']);
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('game_room_id')->unsigned();
            $table->string('note')->nullable();
            $table->timestamps();
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
            $table->foreign('game_room_id')
                ->references('id')
                ->on('game_rooms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_room_user');
    }
}
